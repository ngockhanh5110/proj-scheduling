import os.path, sys
import numpy as np
np.set_printoptions(threshold=sys.maxsize)

def analyse(type_,span,optSpan, schedules,subdir):
    min_span = np.amin(span)
    mean = np.mean(span)
    avg_dev = np.std(span)
    median = np.median(span)

    f_name = 'analyse'+str(len(schedules))+'.txt'
    file_path = os.path.join(subdir, f_name)
    
    # Write Analyse file in a random schedules
    if type_ == "rand":
        with open(file_path, 'a') as t:
            t.write("\nRANDOM SCHEDULES = %s\n" % np.array2string(schedules,separator=','))
            t.write("\nPROJECT SPAN OF RANDOM SCHEDULES = %s\n" % np.array2string(span,separator=','))
            t.write("\nMINIMUM SPAN = %s\n" % min_span)
            t.write("\nSPAN RECORDED = %s\n" % optSpan)
            t.write("\nMEAN SPAN RANDOM = %s\n" % mean)
            t.write("\nMEDIAN SPAN = %s\n" % median)
            t.write("\nSTANDARD DEVIATION = %s\n" % avg_dev)
            t.write("\n======================================================================\n")
            t.close()
    # Write Analyse file in an even schedules
    else:
        with open(file_path, 'a') as t:
            t.write("\nEVENLY DISTRIBUTED SCHEDULES = %s\n" % np.array2string(schedules,separator=','))
            t.write("\nEVENLY DISTRIBUTED SCHEDULES SPAN = %s\n" % np.array2string(span,separator=','))
            t.write("\nMINIMUM SPAN = %s\n" % min_span)
            t.write("\nSPAN RECORDED = %s\n" % optSpan)
            t.write("\nMEAN SPAN EVEN = %s\n" % mean)
            t.write("\nMEDIAN SPAN = %s\n" % median)
            t.write("\nSTANDARD DEVIATION = %s\n" % avg_dev)
            t.close()