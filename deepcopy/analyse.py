import os.path
import statistics 
def analyse(type_,span,optSpan, schedules,subdir):
    min_span = min(span)
    mean = statistics.mean(span)
    avg_dev = statistics.stdev(span)
    median = statistics.median(span)

    f_name = 'analyse'+str(len(schedules))+'.txt'
    file_path = os.path.join(subdir, f_name)
    
    # Write Analyse file in a random schedules
    if type_ == "rand":
        with open(file_path, 'a') as t:
            t.write("\n======================================================================\n")
            t.write("\nRANDOM SCHEDULES = %s\n" % schedules)
            t.write("\nPROJECT SPAN OF RANDOM SCHEDULES = %s\n" % span)
            t.write("\nMINIMUM SPAN = %s\n" % min_span)
            t.write("\nSPAN RECORDED = %s\n" % optSpan)
            t.write("\nMEAN SPAN RANDOM = %s\n" % mean)
            t.write("\nMEDIAN SPAN = %s\n" % median)
            t.write("\nSTANDARD DEVIATION = %s\n" % avg_dev)
            t.close()
    # Write Analyse file in an even schedules
    else:
        with open(file_path, 'a') as t:
            t.write("\n======================================================================\n")
            t.write("\nEVENLY DISTRIBUTED SCHEDULES = %s\n" % schedules)
            t.write("\nEVENLY DISTRIBUTED SCHEDULES SPAN = %s\n" % span)
            t.write("\nMINIMUM SPAN = %s\n" % min_span)
            t.write("\nUPPER BOUND RECORDED = %s\n" % optSpan)
            t.write("\nMEAN SPAN EVEN = %s\n" % mean)
            t.write("\nMEDIAN SPAN = %s\n" % median)
            t.write("\nSTANDARD DEVIATION = %s\n" % avg_dev)
            t.close()