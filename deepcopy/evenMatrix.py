import copy, random
from serialMethod import *
from analyse import *
import gc
def evenMatrix(subOutput,termination,even_dist_schedules,Nb_schedules,total_act,successor,precedent,duration,resource,optSpan,res_availability,answerPath):
    from rand import rand
    n = len(even_dist_schedules)
    even_swap_matrix = [0] * n
    for j in range(n):
        even_swap_matrix[j] = [0] * n
    for stop in termination:
        # start_time_even = time.time()
        for i in range(stop):
            Nb_schedules = len(even_dist_schedules)
            # Calculate the value of swap_matrix
            for x in range(Nb_schedules - 1):
                for y in range(x + 1, Nb_schedules):
                    ordered = even_dist_schedules[x]
                    unsorted = copy.deepcopy(even_dist_schedules[y])
                    swap = 0
                    for i in range(len(unsorted)- 1):
                        if unsorted[i] != ordered[i]:
                            a = unsorted.index(ordered[i])  # Position of job that needs to move up/place in order
                            b = copy.deepcopy(ordered[i])  # Job number to be moved
                            unsorted.remove(unsorted[a])  # Remove job in wrong position
                            unsorted.insert(i, b)  # Insert job number in correct position
                            swap = swap + 1
                    del unsorted, ordered
                    even_swap_matrix[x][y] = swap
            # Base value to begin comparison for minimum value
            minimum1 = even_swap_matrix[0][1]

            for i in range(1, Nb_schedules):
                lst = even_swap_matrix[i]
                for j in range(len(lst)):
                    val = lst[j]
                    if val < minimum1 and val > 0:
                        minimum1 = val
                        row1 = i
                        col1 = j

            # Search for second smallest number in the related column and rows
            # Assign a value as the second smallest number to begin comparison
            minimum2 = minimum1 + 15

            # Search through rows
            # row = row1
            for j in range(Nb_schedules):
                i = even_swap_matrix[row1][j]
                if i == 0:
                    continue
                if i > minimum1 and i < minimum2:
                    minimum2 = i
                    row2 = copy.deepcopy(row1)
                    col2 = j
                if i == minimum1 and j != col1:
                    minimum2 = i
                    row2 = copy.deepcopy(row1)
                    col2 = j
                if i == minimum1 and j == col1:
                    pass

            # row = col1
            for j in range(Nb_schedules):
                i = even_swap_matrix[col1][j]
                if i == 0:  # skips zeros at the beginning
                    continue
                if i > minimum1 and i < minimum2:
                    minimum2 = i
                    row2 = copy.deepcopy(col1)
                    col2 = j
                if i == minimum1:
                    minimum2 = i
                    row2 = copy.deepcopy(col1)
                    col2 = j

            # Search through columns
            # column = col1
            for j in range(Nb_schedules):
                i = even_swap_matrix[j][col1]
                if col1 == 0:  # skip last column
                    break
                if i == 0:  # pass the zeros at the end of a column
                    continue
                if i > minimum1 and i < minimum2:
                    minimum2 = i
                    row2 = j
                    col2 = copy.deepcopy(col1)
                if i == minimum1 and j != row1:
                    minimum2 = i
                    row2 = j
                    col2 = copy.deepcopy(col1)
                if i == minimum1 and j == row1:
                    continue

            # column = row1
            for j in range(Nb_schedules):
                i = even_swap_matrix[j][row1]
                if row1 == 0:  # skip first column of zeros
                    break
                if i == 0:  # pass the zeros at the end of a column
                    continue
                if i > minimum1 and i < minimum2 and i > 0:
                    minimum2 = i
                    row2 = j
                    col2 = copy.deepcopy(row1)
                if i == minimum1 and j != row1:
                    minimum2 = i
                    row2 = j
                    col2 = copy.deepcopy(row1)
                if i == minimum1 and j == row1:
                    continue


            # Determine which schedule to change base on swaps
            location = [row1, col1, row2, col2]
            third_coordinate = []

            # Find the duplicated schedule
            for i in range(len(location) - 1):
                for j in range(i + 1, len(location)):
                    if location[i] == location[j]:
                        possible_schedulechange = location[i]
                        break

            # Creates a list that does not include the duplicated schedule
            for i in range(len(location)):
                if location[i] != possible_schedulechange:
                    third_coordinate.append(location[i])

            # Create a list without duplicates
            no_dup = []
            for i in range(len(location)):
                if location[i] not in no_dup:
                    no_dup.append(location[i])

            # Compare new distance and create new schedule to replace
            for i in range(1):
                if minimum1 != minimum2:
                    # Note the scenario where the third distance = minimum2 creates an isosceles as well
                    # But will fix itself due to the mechanism always picking the minimum distance
                    schedulechange = possible_schedulechange

                if minimum1 == minimum2:
                    # find the third distance
                    third_distance = even_swap_matrix[min(third_coordinate)][max(third_coordinate)]

                    # Isosceles triangle
                    if third_distance != minimum1:
                        schedulechange = possible_schedulechange

                        # In case of equilateral triangle
                    else:
                        # Randomly select a schedule to remove from the no_dup set
                        schedulechange = no_dup[random.randrange(len(no_dup))]

                    # Schedule to remove
                to_remove = copy.deepcopy(even_dist_schedules[schedulechange])
                # Generate new schedule
                Nb_schedules = 1
                new = []
                while len(new) != Nb_schedules:
                    job = 0
                    complete = []
                    eligible = []
                    remaining = [e for e in range(1, total_act + 1)]
                    while len(complete) != total_act:
                        # For normal activities with predecessors
                        if len(precedent[job]) != 0:
                            rand_act = eligible[random.randrange(len(eligible))]
                            precedent_lst = precedent[rand_act - 1]
                            successor_lst = successor[rand_act - 1]
                            # For the dummy activity, the precedent list should be empty
                            if len(precedent_lst) == 0:
                                complete.append(rand_act)
                                eligible.remove(rand_act)
                                for i in successor_lst:
                                    eligible.append(i)
                            # For normal activities
                            else:
                                for elem in precedent_lst:
                                    if elem in complete:
                                        boolean = True
                                    else:
                                        boolean = False
                                        break
                                if boolean == True:
                                    for j in successor_lst:
                                        if j not in eligible:
                                            eligible.append(j)
                                    complete.append(rand_act)
                                    eligible.remove(rand_act)
                                    job = job + 1
                        # For dummy activity
                        else:
                            eligible.append(job + 1)
                            remaining.remove(job + 1)
                            job = job + 1
                    # Start the comparison process, make sure schedules are all unique
                    for i in range(len(to_remove)):
                        if complete[i] == to_remove[i]:
                            boolean = True
                        else:
                            boolean = False
                            new.append(complete)
                            break
                    if boolean == True:
                        break
                    # Calculate distance between new schedule and other two schedules
                    schedule1 = copy.deepcopy(even_dist_schedules[third_coordinate[0]])
                    schedule2 = copy.deepcopy(even_dist_schedules[third_coordinate[1]])
                    tmp = []
                    tmp.append(new[0])
                    tmp.append(schedule1)
                    tmp.append(schedule2)
                    new_dist = []
                    # Compute number of swaps
                    for x in range(len(tmp) - 1):
                        ordered = copy.deepcopy(tmp[0])
                        unsorted = copy.deepcopy(tmp[x + 1])
                        swap = 0
                        for i in range(0, total_act):
                            if unsorted[i] != ordered[i]:
                                a = unsorted.index(ordered[i])  # Position of job that needs to move up / place in order
                                b = copy.deepcopy(ordered[i])  # Job number to be moved
                                unsorted.remove(unsorted[a])  # Remove job in wrong position
                                unsorted.insert(i, b)  # Insert job number in correct position
                                swap = swap + 1
                        new_dist.append(swap)
                    # Make sure new schedule is further than old schedule, otherwise generate new random schedule
                    old_dist = []
                    for i in no_dup:
                        if i != schedulechange:
                            if schedulechange != 0:
                                old_dist.append(even_swap_matrix[min(i,schedulechange)][max(i, schedulechange)])
                            else:
                                old_dist.append(even_swap_matrix[i][schedulechange])
                            # Check whether new schedule is further than old schedule
                    for i in range(len(old_dist)):
                        if new_dist[i] > old_dist[i]:
                            boolean = True
                        else:
                            boolean = False
                            new = []
                            break

                # When new schedule is further than old schedule, remove old and insert new
                if boolean == True:
                    even_dist_schedules.remove(even_dist_schedules[schedulechange])
                    even_dist_schedules.append(new[0])

        even_dist_span = serialMethod(duration,even_dist_schedules,total_act,precedent,resource,res_availability)
        subOutput.append(min(even_dist_span))
        analyse("even",even_dist_span,optSpan,even_dist_schedules,answerPath)
        
        gc.collect()
        if termination.index(stop) == len(termination) -1:
            del even_dist_schedules
            del even_dist_span
            del even_swap_matrix
            del row1
            del row2
            del col1
            del col2
            del to_remove
            del schedule1
            del schedule2
            del ordered
            del unsorted
            del b

            gc.collect()
    return subOutput