
# This file running all the data files so it gonna take a lot of time.
# for testing purpose, please run the singleTest.py on file "j601_1.sm"

from extract import *
from priorityList import *
from serialMethod import *
from evenMatrix import * 
from analyse import *
import csv
import gc
import numpy as np
import copy

import os.path
import shutil

def getOptimalIndex(groupName, dataFile):
    temp_ = dataFile[len(groupName):]
    a = ""
    b = ""
    bol_ = 0
    for i in temp_:
        if i != "_" and bol_ == 0:
            a+=i
        elif i == "_":
            bol_ = 1
        elif i != "." and bol_ == 1:
            b+=i
        elif i == ".":
            break
    return (10 * (int(a)-1)+ int(b)) - 1

import time

start_time = time.time()
groupPaths = []
optimalPaths= []
path = "./data"
for dataGroup in os.listdir(os.path.expanduser(path)):
    if 'j' in dataGroup:
        groupPaths.append(os.path.join(path,dataGroup,dataGroup+".sm"))
        optimalPaths.append(os.path.join(path,dataGroup,dataGroup+"lb.sm"))

groupPaths = ['./data/j120/j120.sm']
optimalPaths = ['./data/j120/j120lb.sm']

for groupPath in groupPaths:
    optimalPath = optimalPaths[groupPaths.index(groupPath)]
    optSpans = extractOptimal(optimalPath)

    # Creating the result file for each data group
    if not os.path.exists(os.path.join(groupPath,'result')):
        os.makedirs(os.path.join(groupPath,'result'))
    for i in range(20, 120, 20):
        with open(os.path.join(groupPath,'result','analyse{}.csv'.format(i)), 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["Project name","Number of rand schedule","Optimal", "Rand Span", "Stop 5 Span", "Stop 10 Span", "Stop 20 Span", "Stop 30 Span"])
    
    # Handle the file analyses
    for dataFile in os.listdir(groupPath):
        if 'j' in dataFile:

            # Step 1: Get the name of data group: "j30", "j60",...
            groupName = ""
            for i in dataFile:
                if i != "0":
                    groupName += i
                else:
                    groupName+= i 
                    break
            
            # Step 2: Set all the data path and the result path
            optimalIndex = getOptimalIndex(groupName,dataFile)
            answerPath = os.path.join(groupPath,"answer", dataFile)
            filePath = os.path.join(groupPath,dataFile)
                # Make the answer folder
                # If there is an existing folder, replace it with a new data
            if not os.path.exists(answerPath):
                os.makedirs(answerPath)
            else:
                shutil.rmtree(answerPath, ignore_errors=False, onerror=None)
                os.makedirs(answerPath)
            
            # Step 3: Extract all the data
            precedent, successor, duration, res_availability, resource, total_act = extractData(filePath)
            optSpan = optSpans[optimalIndex]
            
            # Step 4: Make nested schedules: group of 20, 40, 60, 80 and 100
            schedules_nested = priorityList(dataFile,answerPath,precedent,successor,duration,total_act)

            # Step 5: Analyse on each group of schedules
            for schedules in schedules_nested:
                subOutput = [None] * 4
                subOutput[0] = str(dataFile)
                subOutput[1] = len(schedules)
                subOutput[2] = optSpan
                # Working with random schedules               
                span = serialMethod(duration,schedules,total_act,precedent,resource,res_availability)
                subOutput[3] = min(span)
                analyse("rand",span,optSpan,schedules,answerPath)
                
                # Working with even-distributed schedules
                even_dist_schedules = copy.deepcopy(schedules)
                subOutput = evenMatrix(subOutput,[5,10,20,30],even_dist_schedules,int(subOutput[1]),total_act,successor,precedent,duration,resource,optSpan,res_availability,answerPath)
                
                # Writing csv file
                with open(os.path.join(groupPath,'result','analyse{}.csv'.format(len(schedules))), 'a', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(subOutput)
                del subOutput, span, even_dist_schedules
                gc.collect()
            
end_time = time.time()
print("Its take {} s".format(end_time-start_time))