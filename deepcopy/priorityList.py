def priorityList(data_file,subdir,precedent,successor,duration,total_act):
    import os.path
    import time
    from rand import rand
    schedules_nested = []
    for i in range(20, 120, 20):
        schedules = []
        Nb_schedules = i

        f_name = data_file + '_' + str(Nb_schedules) + '.txt'
        file_path = os.path.join(subdir, f_name)
        t = open(file_path, 'a')

        with open(file_path, 'a') as t:
            t.write("NUMBER OF SCHEDULES %s\n" % Nb_schedules)

        while len(schedules) != Nb_schedules:
            job = 0
            complete = []
            eligible = []
            remaining = [e for e in range(1, total_act + 1)]

            while len(complete) != total_act:

                # FOR NORMAL ACTIVITIES WITH PREDECESSORS

                if len(precedent[job]) != 0:
                    rand_act = eligible[rand(0,len(eligible)-1)]
                    precedent_lst = precedent[rand_act - 1]
                    successor_lst = successor[rand_act - 1]

                    # FOR THE DUMMY ACTIVITY, THE PRECEDENT LIST SHOULD BE EMPTY
                    if len(precedent_lst) == 0:
                        complete.append(rand_act)
                        eligible.remove(rand_act)
                        for i in successor_lst:
                            eligible.append(i)

                    # FOR NORMAL ACTIVITIES
                    else:
                        for elem in precedent_lst:
                            if elem in complete:
                                boolean = True
                            else:
                                boolean = False
                                break
                        if boolean:
                            for j in successor_lst:
                                if j not in eligible:
                                    eligible.append(j)

                            complete.append(rand_act)
                            eligible.remove(rand_act)

                            job = job + 1
                # FOR DUMMY ACTIVITY
                else:
                    eligible.append(job + 1)
                    remaining.remove(job + 1)
                    job = job + 1

            # INSERT THE FIRST COMPLETED SCHEDULE IN THE SET

            schedules.append(complete)
        
        with open(file_path, 'a') as t:
            t.write("\nRANDOM SCHEDULES %s\n" % schedules)  # schedules include all


        schedules_nested.append(schedules)
    return schedules_nested


