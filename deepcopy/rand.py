def rand(lb,ub):
    import random
    import sys
    seedValue = random.randrange(sys.maxsize)
    random.seed(seedValue)
    return random.randint(lb,ub)