def serialMethod(duration,schedules,total_act,precedent,resource,res_availability):
    start_time_nested = []
    end_time_nested = []
    span = []
    longest_span = sum(duration)

    for i in range(len(schedules)):  # for each list in the schedules array
        current_schedule = schedules[i]
        resource_usage = [[0 for col in range(4)] for row in range(longest_span)]
        start_time_array = []
        end_time_array = []

        while len(end_time_array) != total_act:
            for act in current_schedule:
                # For the dummy activity
                if act == 4:
                    next

                if act == 1:
                    start_time_array.append(1)
                    end_time_array.append(1)

                # Check start times for activities
                if len(precedent[act - 1]) != 0:
                    precedents_act = precedent[act - 1]  # extract precedent list
                    pred_end = []
                    for job in precedents_act:
                        if job == 1:
                            poss_start = 0
                        if job != 1:
                            pred_end.append(end_time_array[current_schedule.index(job)])
                            # Extract the end times of precedent
                            poss_start = max(pred_end)
                    # possible start time = the highest end time of precedents

                    tmp = [x + y for x, y in zip(resource_usage[poss_start - 1], resource[act - 1])]

                    # If resources are available at the possible start time
                    # boolean = checkResourcesAtFirst(tmp,res_availability)
                    for i in range(len(tmp)):
                        if tmp[i] <= res_availability[i]:
                            boolean = True
                        else:
                            boolean = False
                            break #LEO: have to return true if ALL elements meet req 

                    if boolean == True:
                        # Check if resource limit is satisfied for the duration of the activity, decide end time
                        # boolean = checkResourcesFollowing(duration,act,resource_usage,poss_start,resource,res_availability)
                        for time_step in range(duration[act - 1]):
                            tmp = [x + y for x, y in zip(resource_usage[poss_start - 1 + time_step], resource[act - 1])]
                            for i in range(len(tmp)):
                                if tmp[i] <= res_availability[i]:
                                    boolean = True
                                else:
                                    boolean = False
                                    break
                            # Exit for loop immediately if resource limit is exceeded at any one time step
                            if boolean == False:
                                break


                    # If earliest start time is not possible, find new start time
                    if boolean == False:
                        # Go to the next time step and find a suitable time to start
                        time_step = 1

                        while boolean == False:
                            tmp = [x + y for x, y in zip(resource_usage[poss_start - 1 + time_step], resource[act - 1])]

                            for i in range(len(tmp)):
                                if tmp[i] <= res_availability[i]:
                                    boolean = True
                                else:
                                    boolean = False
                                    time_step = time_step + 1
                                    break

                            if boolean == True:
                                poss_start = poss_start + time_step  # New possible start
                                for time_step in range(duration[act - 1]):
                                    tmp = [x + y for x, y in zip(resource_usage[poss_start - 1 + time_step], resource[act - 1])]
                                    for i in range(len(tmp)):
                                        if tmp[i] <= res_availability[i]:
                                            boolean = True
                                        else:
                                            boolean = False
                                            break
                                            # Exit for loop immediately if resource limit is exceeded at any one time step
                                    if boolean == False:
                                        break
                    # If boolean is true, then activity can be placed in the space, update all start, end and resource

                    if boolean == True:
                        start_time_array.append(poss_start)
                        end_time = poss_start + duration[act - 1]
                        end_time_array.append(end_time)

                        project_span = max(end_time_array)

                        for time_step in range(duration[act - 1]):
                            resource_usage[poss_start - 1 + time_step] = [(h + i) for h, i in zip(resource_usage[poss_start - 1 + time_step], resource[act - 1])]

        span.append(project_span)
        
    start_time_nested.append(start_time_array)
    end_time_nested.append(end_time_array)
    return span
