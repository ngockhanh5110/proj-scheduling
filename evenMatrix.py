import copy, random, sys
from serialMethod import *
from analyse import *
from rand import rand
import gc, time
import numpy as np
np.set_printoptions(threshold=sys.maxsize)

def evenMatrix(subOutput,termination,even_dist_schedules,Nb_schedules,total_act,successor,precedent,duration,resource,optSpan,res_availability,answerPath):
    even_swap_matrix = np.zeros((Nb_schedules,Nb_schedules), dtype= np.int32)
    avg_ = [None] * 4
    for stop in termination:
        # Make a trash list to contain all past schedule. 
        # dont go back to past schedule
        trash = np.array([np.zeros(total_act,dtype= np.int32)])
        start_ = time.time()
        count_ = 0
        file_path = os.path.join(answerPath, 'analyse'+str(len(even_dist_schedules))+'.txt')
        firsttime = True
        for i in range(stop):
            Nb_schedules = len(even_dist_schedules)
            # Calculate the value of swap_matrix
            for x in range(Nb_schedules - 1):
                for y in range(x + 1, Nb_schedules):
                    ordered = np.copy(even_dist_schedules[x]).tolist()
                    unsorted = np.copy(even_dist_schedules[y]).tolist()
                    swap = 0
                    for i in range(len(unsorted)- 1):
                        if unsorted[i] != ordered[i]:
                            a = unsorted.index(ordered[i])  # Position of job that needs to move up/place in order
                            b = int(np.copy(ordered[i]))  # Job number to be moved
                            unsorted.remove(unsorted[a])  # Remove job in wrong position
                            unsorted.insert(i, b)  # Insert job number in correct position
                            swap = swap + 1
                    del unsorted, ordered
                    even_swap_matrix[x,y] = swap

            # Printing initialized swap_matrix
            if firsttime:
                with open(file_path, 'a') as t:
                    t.write("\nINITALIZED EVEN SWAP MATRIX = %s\n" % np.array2string(even_swap_matrix,separator=','))
                    t.close()
                avg_[1] = np.nanmean(np.where(even_swap_matrix!=0,even_swap_matrix,np.nan))
                firsttime = False

            # Getting the mimimum inside the matrix
            minimum1 = np.amin(even_swap_matrix[even_swap_matrix != 0])
            tempView = np.argwhere(even_swap_matrix == minimum1)
            temp = tempView[rand(0,tempView.shape[0]-1)]
            row1 = temp[0]
            col1 = temp[1]
            del temp

            # Search for second smallest number in the related column and rows
            # Search through rows
            # row = row1
            minimum2 = minimum1 + 15
            tempView = np.array(even_swap_matrix[row1,:])
            tempView_ = tempView[(tempView!= 0) & (tempView!= minimum1) & (tempView <= minimum2)]
            if tempView_.size != 0:
                minimum2 = np.amin(tempView_)
                tempView = np.argwhere(tempView == minimum2)
                temp = tempView[rand(0,tempView.shape[0]-1)]
                row2 = int(np.copy(row1))
                col2 = temp[0]
                del temp
            del tempView_
            del tempView
            
            # row = col1
            tempView = np.array(even_swap_matrix[col1,:])
            tempView_ = tempView[(tempView!= 0) & (tempView!= minimum1) & (tempView <= minimum2)]
            if tempView_.size != 0:
                minimum2 = np.amin(tempView_)
                tempView = np.argwhere(tempView == minimum2)
                temp = tempView[rand(0,tempView.shape[0]-1)]
                row2 = int(np.copy(col1))
                col2 = temp[0]
                del temp
            del tempView
            del tempView_

            # Search through columns
            # column = col1
            tempView = np.array(even_swap_matrix[:,col1])
            tempView_ = tempView[(tempView!= 0) & (tempView!= minimum1) & (tempView <= minimum2)]
            if tempView_.size != 0:
                minimum2 = np.amin(tempView_)
                tempView = np.argwhere(tempView == minimum2)
                temp = tempView[rand(0,tempView.shape[0]-1)]
                row2 = temp[0]
                col2 = int(np.copy(col1))
                del temp
            del tempView
            del tempView_


            # column = row1
            tempView = np.array(even_swap_matrix[:,row1])
            tempView_ = tempView[(tempView!= 0) & (tempView!= minimum1) & (tempView <= minimum2)]
            if tempView_.size != 0:
                minimum2 = np.amin(tempView_)
                tempView = np.argwhere(tempView == minimum2)
                temp = tempView[rand(0,tempView.shape[0]-1)]
                row2 = temp[0]
                col2 = int(np.copy(row1))
                del temp
            del tempView
            del tempView_

            # Determine which schedule to change base on swaps
            location = np.array([row1, col1, row2, col2])

            # Find the duplicated schedule
            possible_schedulechange = [x for x in location if location.tolist().count(x)> 1][0]

            # Creates a list that does not include the duplicated schedule
            third_coordinate = np.array([x for x in location if x!= possible_schedulechange])
            
            # Create a list without duplicates
            no_dup = np.unique(location)
            del location

            # Compare new distance and create new schedule to replace
            for i in range(1):
                if minimum1 != minimum2:
                    # Note the scenario where the third distance = minimum2 creates an isosceles as well
                    # But will fix itself due to the mechanism always picking the minimum distance
                    schedulechange = possible_schedulechange

                else:
                    # find the third distance
                    third_distance = even_swap_matrix[np.amin(third_coordinate),np.amax(third_coordinate)]
                    del third_coordinate

                    # Isosceles triangle
                    if third_distance != minimum1:
                        schedulechange = possible_schedulechange

                        # In case of equilateral triangle
                    else:
                        # Randomly select a schedule to remove from the no_dup set
                        schedulechange = no_dup[rand(0,no_dup.size-1)]



                    # Schedule to remove
                
                to_remove = np.copy(even_dist_schedules[schedulechange])
                trash = np.append(trash,[to_remove],axis = 0)

                # Generate new schedule
                Nb_schedules = 1
                new = []
                while len(new) != Nb_schedules:
                    job = 0
                    complete = np.array([], np.int32)
                    eligible = np.array([], np.int32)
                    remaining = np.arange(1,total_act+1,dtype= np.int32)
                    
                    # Serial method
                    while complete.shape[0] != total_act:
                        # For normal activities with predecessors
                        if len(precedent[job]) != 0:
                            rand_act = eligible[rand(0,eligible.size-1)]
                            precedent_lst = np.array(precedent[rand_act - 1])
                            successor_lst = np.array(successor[rand_act - 1])
                            # For the dummy activity, the precedent list should be empty
                            if precedent_lst.shape[0] == 0:
                                complete = np.append(complete, rand_act)
                                eligible = np.delete(eligible, np.where(eligible == rand_act))
                                eligible = np.append(eligible, successor_lst)
                    
                            # For normal activities
                            else:
                                tempView = np.intersect1d(complete,precedent_lst)
                                if tempView.size == precedent_lst.size and (tempView == precedent_lst).all() :
                                    eligible = np.append(eligible, np.setdiff1d(successor_lst,eligible))
                                    complete = np.append(complete, rand_act)
                                    eligible = np.delete(eligible, np.where(eligible == rand_act))
                                    job = job + 1
                                    boolean = True
                                else:
                                    boolean = False
                                del tempView
                        # For dummy activity
                        else:
                            eligible = np.append(eligible,job+1)
                            remaining = np.delete(remaining, np.where(remaining == job + 1))
                            job = job + 1
                    del eligible, remaining, precedent_lst,successor_lst
                    
                    # Start the comparison process, make sure schedules are all unique
                    # Leo: by any chance, the new schedule will get back to the to_remove schedule?
                    for i in trash:
                        if (complete == i).all():
                            boolean = True
                            break
                        else:
                            boolean = False
                    count_ += 1
                    if boolean:
                        boolean = True
                        break
                    else:    
                        boolean = False
                        new.append(complete)
                        tmp = np.array([complete, even_dist_schedules[third_coordinate[0]],even_dist_schedules[third_coordinate[1]]])
                        
                    
                    # Calculate distance between new schedule and other two schedules
                    new_dist = np.zeros((len(tmp)-1,),dtype = np.int32 )
                    # Compute number of swaps
                    for x in range(len(tmp) - 1):
                        ordered = np.copy(tmp[0]).tolist()
                        unsorted = np.copy(tmp[x+1]).tolist()
                        swap = 0
                        for i in range(0, total_act):
                            if unsorted[i] != ordered[i]:
                                a = unsorted.index(ordered[i])  # Position of job that needs to move up / place in order
                                b = int(np.copy(ordered[i]))  # Job number to be moved
                                unsorted.remove(unsorted[a])  # Remove job in wrong position
                                unsorted.insert(i, b)  # Insert job number in correct position

                                swap = swap + 1
                        del ordered, unsorted
                        new_dist[x] = swap
                    # Make sure new schedule is further than old schedule, otherwise generate new random schedule
                    old_dist = []
                    for i in no_dup:
                        if i != schedulechange:
                            if schedulechange != 0:
                                old_dist.append(even_swap_matrix[min(i,schedulechange),max(i, schedulechange)])
                            else:
                                old_dist.append(even_swap_matrix[i,schedulechange])
                            # Check whether new schedule is further than old schedule
                    old_dist = np.array(old_dist)
                    
                    if (new_dist> old_dist).all():
                        boolean = True
                    else:
                        boolean = False
                        new = []
                    
                    del new_dist, old_dist

                    # When new schedule is further than old schedule, remove old and insert new
                    if boolean:
                        even_dist_schedules[schedulechange] = np.copy(new[0])
                        
                    gc.collect()

        even_dist_span = serialMethod(duration,even_dist_schedules,total_act,precedent,resource,res_availability)
        subOutput.append(min(even_dist_span))
        analyse("even",even_dist_span,optSpan,even_dist_schedules,answerPath)
        avg_[2] = np.nanmean(np.where(even_swap_matrix!=0,even_swap_matrix,np.nan))
        stop_ = time.time()
        
        with open(file_path, 'a') as t:
            t.write("\nADJUSTED EVEN SWAP MATRIX = %s\n" % np.array2string(even_swap_matrix,separator=','))
            t.write("\nNUMBER OF STEPS JUMPING = %s\n" % (count_))
            t.write("\nTIME TAKEN THIS STOP = %s\n" % (stop_-start_))
            t.write("\n======================================================================\n")
            t.close()        

        # Find and printout the closest points' distance
        if termination.index(stop) == len(termination) - 1:
            _nbschedules = round(0.15 * len(even_dist_schedules))
            _lts = np.copy(even_dist_span)
            _lts = np.unique(np.sort(_lts, axis= None))
            _schedules = []
            t = 0
            while len(_schedules) < _nbschedules:
                i = 0
                _templts = np.where(even_dist_span == _lts[t])[0]
                for k in np.where(even_dist_span == _lts[t])[0]:
                    _schedules.append(even_dist_schedules[k])
                    if len(_schedules) == _nbschedules:
                        break
                t +=1
                if len(_schedules) == _nbschedules:
                        break


            _schedules = np.array(_schedules)
            _swap_matrix = np.zeros((_nbschedules,_nbschedules), dtype= np.int32)
            
             # Calculate the value of swap_matrix
            for x in range(_nbschedules - 1):
                for y in range(x + 1, _nbschedules):
                    ordered = np.copy(_schedules[x]).tolist()
                    unsorted = np.copy(_schedules[y]).tolist()
                    swap = 0
                    for i in range(len(unsorted)- 1):
                        if unsorted[i] != ordered[i]:
                            a = unsorted.index(ordered[i])  # Position of job that needs to move up/place in order
                            b = int(np.copy(ordered[i]))  # Job number to be moved
                            unsorted.remove(unsorted[a])  # Remove job in wrong position
                            unsorted.insert(i, b)  # Insert job number in correct position
                            swap = swap + 1
                    del unsorted, ordered
                    _swap_matrix[x,y] = swap

            file_path = os.path.join(answerPath, 'analyse'+str(len(even_dist_schedules))+'.txt')

            with open(file_path, 'a') as t:
                t.write("\nMINIMUM SCHEDULES = %s\n" % np.array2string(_schedules,separator=','))
                t.write("\nMINIMUM SCHEDULES' SWAP MATRIX = %s\n" % np.array2string(_swap_matrix,separator=','))
                t.close()
            
            avg_[3] = np.nanmean(np.where(_swap_matrix!=0,_swap_matrix,np.nan))

    return subOutput, avg_