import os.path
import re
import numpy as np
import sys

def extractFromFile(data_file):
    # READ FILE EXTRACT DATA
    f = open(data_file, 'rt')
    content = []
    line = f.readline()
    for line in f:
        content.append(line)  # all lines read from file stored in content
    f.close()
    return np.array(content)


def extractData(data_file):
    
    filecount = 0        

    content = extractFromFile(data_file)

    # TOTAL NUMBER OF JOBS IN PROJECT
    act_line = list(filter(str.isdigit, content[4]))
    str_ = ""
    for _ in act_line:
        str_ += _
    total_act = int(str_)
    
    # EXTRACTION OF PRECEDENCE AND SUCCESSOR DATA
    data_st = 17
    data_end = data_st + total_act - 1
    row = data_st

    # DATA STORED IN ARRAY
    psarray = []
    while row <= data_end:
        psdata = list(map(int, list(re.findall('\d+', content[row]))))
        psarray.append(psdata)
        row = row + 1

    # PREDECESSOR ARRAY
    precedent = [[] for _ in range(total_act)]
    successor = [[] for _ in range(total_act)]

    pre_act = 0
    for a in psarray:
        for b in range(len(a)):
            if b in range(3, 6):
                tmp = a[b]
                successor[pre_act].append(tmp)
                precedent[tmp - 1].append(pre_act + 1)
        pre_act = pre_act + 1
    #NOTE: Add 1 to all the activity numbers in precedent


    # EXTRACTION OF ACTIVITY DURATION AND RESOURCE USE DATA
    dt_st = data_end + 5
    dt_ed = dt_st + total_act - 1
    rw = dt_st
    dures = []

    while rw <= dt_ed:
        dures_data = list(map(int, list(re.findall('\d+', content[rw]))))
        dures.append(dures_data)
        rw = rw + 1

    duration = np.zeros((total_act,), dtype= int)
    resource = np.zeros((total_act,4), dtype= int)

    act = 0
    for c in dures:
        duration[act] = c[2]
        resource[act,0] = c[3]
        resource[act,1] = c[4]
        resource[act,2] = c[5]
        resource[act,3] = c[6]
        act = act + 1
    
    # RESOURCE AVAILABILITY
    res_line = dt_ed + 4
    res_avail = list(map(int, list(re.findall('\d+', content[res_line]))))
    res_availability = np.array(res_avail, np.int32)
    
    return precedent, successor, duration, res_availability, resource, total_act


def extractOptimal(data_file):
    # CURRENT OPTIMUM SOLUTION
    try:
        f = open(data_file, 'rt')
        line = f.readline()
    except UnicodeDecodeError:
        f = open(data_file, 'rt', encoding = "ISO-8859-1")
        line = f.readline()
    
    opt_content = []
    opt = []
    
    for line in f:
        temp_ = list(map(int, list(re.findall('\d+', line))))
        if len(temp_) != 0:
            opt.append(temp_)
    f.close()

    opt_span = np.array([i[2] for i in opt])

    return opt_span


    
    