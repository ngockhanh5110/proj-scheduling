import numpy as np
def serialMethod(duration,schedules,total_act,precedent,resource,res_availability):
    span = np.zeros((len(schedules),), dtype=np.int32)
    longest_span = sum(duration)

    for i in range(len(schedules)):  # for each list in the schedules array
        current_schedule = schedules[i]
        
        resource_usage = np.zeros((longest_span,4), dtype = np.int32)
        end_time_array = []

        while len(end_time_array) != total_act:
            for act in current_schedule:
                # For the dummy activity
                if act == 4:
                    next

                if act == 1:
                    end_time_array.append(1)

                # Check start times for activities
                if len(precedent[act - 1]) != 0:
                    precedents_act = precedent[act - 1]  # extract precedent list
                    pred_end = []
                    for job in precedents_act:
                        if job == 1:
                            poss_start = 0
                        if job != 1:
                            pred_end.append(end_time_array[current_schedule.tolist().index(job)])
                            # Extract the end times of precedent
                            poss_start = max(pred_end)
                    # possible start time = the highest end time of precedents

                    
                    tmp = np.add(resource_usage[poss_start - 1], resource[act - 1])
                    # If resources are available at the possible start time
                    boolean = (tmp<= res_availability).all()

                    if boolean:
                        # Check if resource limit is satisfied for the duration of the activity, decide end time
                        for time_step in range(duration[act - 1]):
                            tmp = np.add(resource_usage[poss_start - 1 + time_step], resource[act - 1])
                            boolean = (tmp<= res_availability).all()
                            # Exit for loop immediately if resource limit is exceeded at any one time step
                            if not boolean:
                                break


                    # If earliest start time is not possible, find new start time
                    if not boolean:
                        # Go to the next time step and find a suitable time to start
                        time_step = 1

                        while not boolean:
                            tmp = np.add(resource_usage[poss_start - 1 + time_step], resource[act - 1])
                            boolean = (tmp<= res_availability).all()
                            if not boolean:
                                time_step = time_step + 1

                            if  True:
                                poss_start = poss_start + time_step  # New possible start
                                for time_step in range(duration[act - 1]):
                                    tmp = np.add(resource_usage[poss_start - 1 + time_step], resource[act - 1])
                                    boolean = (tmp<= res_availability).all()
                                    if boolean == False:
                                        break
                    # If boolean is true, then activity can be placed in the space, update all start, end and resource

                    if boolean == True:
                        end_time = poss_start + duration[act - 1]
                        end_time_array.append(end_time)

                        project_span = max(end_time_array)

                        for time_step in range(duration[act - 1]):
                            resource_usage[poss_start - 1 + time_step] = np.add(resource_usage[poss_start - 1 + time_step], resource[act - 1])

        span[i] = project_span
        
    return span