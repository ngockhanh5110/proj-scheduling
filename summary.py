import sys
import os
import re
import numpy as np
import csv
import concurrent.futures

# This function will return a list of links direct to the analyse100.txt


def filesGathering():
    paths = []
    for folder in os.listdir(os.path.expanduser("./data")):
        if "j" in folder:
            try:
                for file in os.listdir(os.path.join("./data", folder, folder+".sm", "answer")):
                    if "j" in file:
                        path = os.path.join(
                            "./data", folder, folder+".sm", "answer", file, "analyse100.txt")
                        paths.append(path)
                if not os.path.exists(os.path.join("./data", folder, folder+".sm",'result')):
                    os.makedirs(os.path.join("./data", folder, folder+".sm",'result'))
                
                #Initialize new result file
                with open(os.path.join("./data", folder, folder+".sm",'result','Min Dist.csv'), 'w', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(["Project name","Rand", "Stop 50", "Stop 100", "Stop 500"])
                    file.close()
                with open(os.path.join("./data", folder, folder+".sm",'result','Avg Dist.csv'), 'w', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(["Project name","Rand", "Stop 50", "Stop 100", "Stop 500"])
                    file.close()
                with open(os.path.join("./data", folder, folder+".sm",'result','Min Span.csv'), 'w', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(["Project name", "Optimal","Rand", "Stop 50", "Stop 100", "Stop 500"])
                    file.close()
                with open(os.path.join("./data", folder, folder+".sm",'result','Avg Span.csv'), 'w', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(["Project name", "Optimal","Rand", "Stop 50", "Stop 100", "Stop 500"])
                    file.close()
            except FileNotFoundError:
                pass
    return paths


# This function will return all data had been stored in the text file into its data type.
def dataExtracting(path):
    with open(path) as file:
        data = file.readlines()
    # Initialize value
    initial = False
    adjustedDistMatrix = []
    minSpans = []
    meanSpans = []
    
    i = 0
    while i < len(data):
        # Initial distance matrix data
        if data[i].startswith("INITALIZED EVEN SWAP MATRIX = ") and not initial:
            currentLine = data[i]
            i+=1
            while data[i] != '\n':
                currentLine = ','.join([currentLine,data[i]])
                i+=1
            # Return value
            initialDistMatrix = np.array(list(map(int, list(re.findall('\d+', currentLine)))))
            initial = True
        # Adjusted distance matrix data
        elif data[i].startswith("ADJUSTED EVEN SWAP MATRIX = "):
            currentLine = data[i]
            i+=1
            while data[i] != '\n':
                currentLine = ','.join([currentLine,data[i]])
                i+=1
            # Return value
            adjustedDistMatrix.append(np.array(list(map(int, list(re.findall('\d+', currentLine))))))
        # Optimal span
        elif data[i].startswith("SPAN RECORDED"):
            currentLine = data[i]
            i+=1
            while data[i] != '\n':
                currentLine = ','.join([currentLine,data[i]])
                i+=1
            # Return value
            optimalSpan = list(map(int, list(re.findall('\d+', currentLine))))[0]
        # Min spans
        elif data[i].startswith("MINIMUM SPAN "):
            currentLine = data[i]
            i+=1
            while data[i] != '\n':
                currentLine = ','.join([currentLine,data[i]])
                i+=1
            # Return value
            minSpans.append(list(map(int, list(re.findall('\d+', currentLine))))[0])
        # Min spans
        elif data[i].startswith("MEAN SPAN "):
            currentLine = data[i]
            i+=1
            while data[i] != '\n':
                currentLine = ','.join([currentLine,data[i]])
                i+=1
            # Return value
            meanSpans.append(list(map(int, list(re.findall('\d+', currentLine))))[0])
        else:
            i+=1  
    return initialDistMatrix,adjustedDistMatrix,optimalSpan,minSpans,meanSpans

def getResultPath(path):
    lts = path.split("/")
    return os.path.join(lts[0],lts[1],lts[2],lts[3],"result")

# This function will handle writing file of Minimum Distance
def minDistWriting(path,initialDistMatrix,adjustedDistMatrix):
    with open(os.path.join(getResultPath(path),'Min Dist.csv'), 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([path.split("/")[-2],
                        sorted(set(initialDistMatrix))[1], 
                        sorted(set(adjustedDistMatrix[0]))[1], 
                        sorted(set(adjustedDistMatrix[1]))[1], 
                        sorted(set(adjustedDistMatrix[2]))[1]])
        file.close()

# This function will handle mean calculation of an array which has zero elements
def calMean(arr):
    return np.nanmean(np.where(arr!=0,arr,np.nan))

# This function will handle writing file of Average Distance
def avgDistWriting(path,initialDistMatrix,adjustedDistMatrix):
    with open(os.path.join(getResultPath(path),'Avg Dist.csv'), 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([path.split("/")[-2],
                        calMean(np.array(initialDistMatrix)), 
                        calMean(adjustedDistMatrix[0]), 
                        calMean(adjustedDistMatrix[1]),
                        calMean(adjustedDistMatrix[2])])
        file.close()

# This function will handle writing file of Minimum Span
def minSpanWriting(path,optimalSpan,minSpans):
    with open(os.path.join(getResultPath(path),'Min Span.csv'), 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([path.split("/")[-2],
                        optimalSpan,
                        minSpans[0], 
                        minSpans[1], 
                        minSpans[2],
                        minSpans[3]])
        file.close()

# This function will handle writing file of Average Span
def avgSpanWriting(path,optimalSpan,meanSpans):
    with open(os.path.join(getResultPath(path),'Avg Span.csv'), 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([path.split("/")[-2],
                        optimalSpan,
                        meanSpans[0], 
                        meanSpans[1], 
                        meanSpans[2],
                        meanSpans[3]])
        file.close()

# This function will serve as main part to handle concurrency
def concurrencyHandler(path):
    try:
        initialDistMatrix,adjustedDistMatrix,optimalSpan,minSpans,meanSpans = dataExtracting(path)
        minDistWriting(path,initialDistMatrix,adjustedDistMatrix)
        avgDistWriting(path,initialDistMatrix,adjustedDistMatrix)
        minSpanWriting(path,optimalSpan,minSpans)
        avgSpanWriting(path,optimalSpan,meanSpans)
    except FileNotFoundError:
        print("Cannot find the file(s)")

# Main function
if __name__ == "__main__":
    paths = filesGathering()
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(concurrencyHandler,paths)
    
